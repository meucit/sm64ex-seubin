#!/usr/bin/env bash

if [[ $1 = "clean" ]]; then
    rm -rf ./build
    rm -rf ./emsdk
    cd ./sm64ex-src
    make distclean
    cd tools
    make clean
    exit
fi

if [[ $2 = "wpkg" ]]; then
    [[ -d ./build ]] || ./build.sh ${1}
    [[ -d ./wpkg ]] || mkdir ./wpkg
    mkdir ./wpkg/sbfs
    cp -a ./build/sm64pc.sm64ex.sbfs/* ./wpkg/sbfs/
    touch ./wpkg/WHERE
    echo "things" > ./wpkg/WHERE
    touch ./wpkg/SBFSNAME
    echo "sm64pc.sm64ex" > ./wpkg/SBFSNAME
    cd ./wpkg
    zip -r ../sm64pc.sm64ex.wpkg ./ > /dev/null
    cd ../
    exit
fi

[ -z "$1" ] && echo "MISTAKE: Add path to the baserom as the first argument" && exit

    git clone https://github.com/emscripten-core/emsdk
    cd emsdk
    ./emsdk install 1.39.5
    ./emsdk activate 1.39.5
    source "./emsdk_env.sh"
    cd ..

    git clone https://gitea.com/meucit/sm64ex-seubin -b wasm
    cd sm64ex-seubin
    cp $1 ./baserom.us.z64
    echo "Compiling SM64ex for Emscripten"
    make -j4
    cd ..

    echo "Creating .sbfs folder."
    mkdir build
    mkdir build/sm64pc.sm64ex.sbfs
    mkdir build/sm64pc.sm64ex.sbfs/data

    cp -r src/* build/sm64pc.sm64ex.sbfs
    cp -r sm64ex-seubin/build/us_web/sm64.us.f3dex2e.wasm build/sm64pc.sm64ex.sbfs/data/game.wasm
    cp -r sm64ex-seubin/build/us_web/sm64.us.f3dex2e.wasm.map build/sm64pc.sm64ex.sbfs/data/game.wasm.map
    cp -r sm64ex-seubin/build/us_web/sm64.us.f3dex2e.js build/sm64pc.sm64ex.sbfs/js/loadgame.js
    # Patch loadgame.js so it points to "game.wasm" instead of "sm64.us.f3dex2e.wasm"
    patch -u  -i fsredir.patch build/sm64pc.sm64ex.sbfs/js/loadgame.js
    rm build/sm64pc.sm64ex.sbfs/js/loadgame.js.orig
    echo "Adding seubin metadata..."
    cp -r seubin.meta/* build/sm64pc.sm64ex.sbfs
